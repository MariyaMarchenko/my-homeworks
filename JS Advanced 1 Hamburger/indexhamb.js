function Hamburger(size, stuffing) {
    this.size = size;
    this.stuffing = stuffing;
    this.toppings = [];
    this.price = size.price += stuffing.price;
    this.ccal = size.ccal += stuffing.ccal;


}

Hamburger.SIZE_SMALL = {
    name: 'small',
    price: 50,
    ccal: 20
};

Hamburger.SIZE_LARGE = {
    name: 'large',
    price: 100,
    ccal: 40
};

Hamburger.STUFFING_CHEESE = {
    name: 'cheese',
    price: 10,
    ccal: 20
};

Hamburger.STUFFING_SALAD = {
    name: 'salad',
    price: 20,
    ccal: 5
};

Hamburger.STUFFING_POTATO = {
    name: 'potato',
    price: 15,
    ccal: 10
};

Hamburger.TOPPING_SPICE = {
    name: 'spice',
    price: 15,
    ccal: 0
};

Hamburger.TOPPING_MAYO = {
    name: 'mayo',
    price: 20,
    ccal: 5
};


Hamburger.prototype.getToppings = function (){
    if (!this.toppings.length) return "NO TOPPINGS ADDED";
    return this.toppings

}

Hamburger.prototype.getStuffing = function () {
    return this.stuffing
}


Hamburger.prototype.getSize = function (){
    return this.size
}


Hamburger.prototype.addTopping = function (topping) {
    for (let topp of this.toppings) {
        if (topp.name === topping.name) throw new Error("topping already added");
    }

    this.toppings.push(topping);

};

Hamburger.prototype.removeTopping = function (topping){
    if(this.toppings.length === 0) throw new Error("There are no toppings");
    let added = this.toppings.map(top => top.name);
    if (!added.includes(topping.name)) throw  new Error("There is no such topping");
    this.toppings = this.toppings.filter( item =>  item.name !== topping.name)
};

Hamburger.prototype.calculatePrice = function () {
    let arrTopp = this.toppings
    let price = 0
    arrTopp.forEach(function (top) {
        price += top.price
    })
    return price + this.price
};

Hamburger.prototype.calculateCalories = function () {
    let arrayTopp = this.toppings
    let ccal = 0
    arrayTopp.forEach(function (topp) {
        ccal += topp.ccal
    })

    return ccal += this.ccal
};


let burger = new Hamburger(Hamburger.SIZE_LARGE, Hamburger.STUFFING_SALAD);
burger.addTopping(Hamburger.TOPPING_SPICE);
console.log(burger.getStuffing());
console.log (burger.getSize());
console.log (burger.getToppings());
console.log(burger.calculatePrice());
console.log(burger.calculateCalories());
console.log (burger.removeTopping(Hamburger.TOPPING_SPICE));
// console.log (burger.removeTopping(Hamburger.TOPPING_MAYO));
console.log(burger.calculatePrice());
console.log(burger.calculateCalories());



console.log(burger);



// console.log("Price: %f", burger.calculatePrice());
// console.log("Calories: %f", burger.calculateCalories());

