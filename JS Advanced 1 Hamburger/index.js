// let userArray = [1, "Matelda", "Bukowski", "mbukowski1@ask.com", "Female", "063-8231212"]
// let[ , name, surname] = userArray
// console.log(name);
// console.log(surname);
//
// let [id, ...rest] =userArray;
// console.log(id);
// console.log(...rest);

// let [ , , , , , tel] = userArray;
// console.log(tel);
//
// let [ code] = tel.split('-');
// console.log(code);

// let userObj = {
// //     "id": 1,
// //     "first_name": "Matelda",
// //     "last_name": "Bukowski",
// //     "gender": "Female",
// //     "contacts": {
// //         "phone": "0638231212",
// //         "email": "mbukowski0@ask.com"
// //     }
// // }
// //
// // let {first_name, last_name} = userObj;
// // console.log(first_name);
// // console.log(last_name);
// // let {contacts:rest} = userObj;
// //
// // console.log(rest);
// //
// // let {gender: g} =userObj;
// // console.log(g);
// //
// //  let {contacts:{email, fax = false}} = userObj;
// //
// // console.log(email);
// // console.log(fax);

// function fighter() {
//     let hp = 100
//     return function hit() {
//         hp-=10
//     console.log(hp);
//     }
//
// }
//
// let  afan = fighter();
// afan();
// afan();
// afan();
//

// let animal = {
// //     eats:true
// // }
// //
// // let rabbit = {
// //     jumps:true
// // }
// // rabbit.__proto__=animal
// // console.log(rabbit.eats);

// let food = {
//     cold: true,
//     taste: true,
// };
//
// let burger = {
//     meat: true,
//     cheese: true,
//     name: 'cheeseburger',
// };
//
// burger.__proto__ = food
//
// console.log(burger.cold);

// let food = {
//     warming() {
//         this.cold = false
//     }
// }

// function burger() {
// //     this.cold = true
// //     this.ready = function () {
// //         return !this.cold
// //
// //     }
// //
// // }
// //
// // burger.prototype = food
// // let cheeseburger = new burger
// //
// // let bigMac = new burger
// //
// // console.log(cheeseburger.cold);
// //
// // cheeseburger.warming()
// // console.log(cheeseburger.cold);
// //
// // console.log(bigMac.cold)


// Написать реализацию функции, которая позволит создавать объекты типа Hamburger, используя возможности стандарта ES5.
//
//     Технические требования:
//
//
//     Некая сеть фастфудов предлагает два вида гамбургеров:
//
//     маленький (50 гривен, 20 калорий)
// большой (100 гривен, 40 калорий)
//
//
//
// Гамбургер должен включать одну дополнительную начинку (обязательно):
//
// сыр (+ 10 гривен, + 20 калорий)
// салат (+ 20 гривен, + 5 калорий)
// картофель (+ 15 гривен, + 10 калорий)
//
//
//
// Дополнительно, в гамбургер можно добавить приправу (+ 15 гривен, 0 калорий) и полить майонезом (+ 20 гривен, + 5 калорий).
//
//
// Необходимо написать программу, рассчитывающую стоимость и калорийность гамбургера. Обязательно нужно использовать ООП подход (подсказка: нужен класс Гамбургер, константы, методы для выбора опций и рассчета нужных величин).
//
//
// Код необходимо написать под стандарт ES5.
//
//
//     Код должен быть защищен от ошибок. Представьте, что вашим классом будет пользоваться другой программист. Если он передает неправильный тип гамбургера, например, или неправильный вид добавки, должно выбрасываться исключение (ошибка не должна молча игнорироваться).
//
//
// Написанный класс должен соответствовать следующему jsDoc описанию (то есть содержать указанные методы, которые принимают и возвращают данные указанного типа и выбрасывают исключения указанного типа. Комментарии ниже тоже можно скопировать в свой код):

// function Hamburger(size, stuffing) {
//
//     if (size = "large"){
//         this.price=100
//     }else {
//         this.price=50
//     }
//    return this
// }
// Hamburger.SIZE_SMALL = 'small';
// Hamburger.SIZE_LARGE = 'large';
//
// let burger  = new Hamburger(Hamburger.SIZE_LARGE);
// console.log(burger);



// function Hamburger(size, stuffing = false) {
//     this.price = size.price
//     if(stuffing)this.price += stuffing.price
//     this.ccal = size.ccal + stuffing.ccal;
// }
//
//    Hamburger.SIZE_SMALL = {
//     price: 50,
//     ccal: 20,
// };
//
// Hamburger.SIZE_LARGE = {
//     price: 100,
//     ccal: 40,
// } ;
//
// Hamburger.STUFFING_CHEESE = {
//     price: 10,
//     ccal: 20,
// } ;
//
// Hamburger.STUFFING_SALAD = {
//     price: 20,
//     ccal: 5,
// } ;
//
// Hamburger.STUFFING_POTATO = {
//     price: 15,
//     ccal: 10,
// } ;
//
// Hamburger.TOPPING_SPICE = {
//     price: 15,
//     ccal: 0,
// };
//
// Hamburger.TOPPING_MAYO = {
//     price: 20,
//     ccal: 5,
// }
//
//     Hamburger.prototype.calculatePrice = function () {
//     return this.price
// }
//
//
// Hamburger.prototype.calculateCalories = function (){
//     return this.ccal
// }
//
// Hamburger.prototype.addTopping = function (topping){
//    this.price += topping.price
//     this.ccal += topping.ccal
// }
//
// let burger  = new Hamburger(Hamburger.SIZE_LARGE, Hamburger.STUFFING_CHEESE );
// burger.addTopping(Hamburger.TOPPING_MAYO)
//
// console.log("Price: %f", burger.calculatePrice());
// console.log("Calories: %f", burger.calculateCalories());
// console.log(burger);




// const constantValues = {
//     SIZE_SMALL : {
//         price: 15,
//         cal: 250,
//     },
//
//     SIZE_LARGE : {
//         price: 25,
//         cal: 340,
//     },
//
//     STUFFING_CHEASE : {
//         price: 4,
//         cal: 25,
//     },
//
//     STUFFING_SALAD : {
//         price: 5,
//         cal: 5,
//     },
//
//     STUFFING_BEEF : {
//         price: 10,
//         cal: 50,
//     },
// };


/*ЗАДАНИЕ 2
ПЕРЕДЕЛАТЬ задание 1 следующим образом:
0 - "запаковываем" весь функционал в один объект
1 - пользовательский ввод размера и начинки теперь сохраняется В СВОЙСТВА ОБЪЕКТА size и stuffing. И теперь любое обращение к ним должно быть осуществлено как к свойству объекта.
2 - обе функции для вычисления каллорийности и цены являются МЕТОДАМИ ОБЪЕКТА, соответственно они внутри себя оперируют значениями СВОЙСТВ объекта в котором находятся.
*/
// let Burger = {
//     size: prompt('write size for your burger', 'SIZE_SMALL')
// }
//
// const validation = (burgerSize,  burgerStaffing) => {
//     while ('' === burgerStaffing || '' === burgerSize ){
//         burgerSize= prompt('enter burger size','SIZE_SMALL');
//         burgerStaffing = prompt("enter burger stuff",'STUFFING_CHEASE');
//     }
//     return {
//         size: burgerSize,
//         stuffing: burgerStaffing
//     }
// };
//
// function priceCalcAndCalories({size, stuffing}) {
//     return {
//         resPrice : constantValues [size].price + constantValues [stuffing].price,
//         resCalories: constantValues [size].cal + constantValues[stuffing].cal
//     };
// }
//
//
// const getBurgerPriceAndCalories = () => {
//     const burgerSize = prompt("enter burger size", 'SIZE_SMALL'),
//         burgerStaffing = prompt("enter burger stuff", 'STUFFING_CHEASE');
//     const validated = validation(burgerSize,burgerStaffing);
//     const result = priceCalcAndCalories (validated) ;
//     return `price of your burger - ${result.resPrice}
//     calories of your burger - ${result.resCalories}`
// };
//
// console.log(getBurgerPriceAndCalories()
// );
//
//
// let Burger = {
//     size:prompt('enter burger size','SIZE_SMALL'),
//     stuffing: prompt('enter burger stuff', 'STUFFING_CHEASE'),
//     price:0,
//     cal:0,
//     priceCalcAndCalories:function () {
//         this.price = constantValues [this.size].price + constantValues [this.stuffing].price;
//         this.cal = constantValues [this.size].cal + constantValues [this.stuffing].cal
//     },
//     validation : function () {
//         while (''=== this.size || '' === this.stuffing) {
//             this.size = prompt( 'write size for your buger', 'SIZE_SMALL');
//             this.stuffing = prompt('write stuffing for your burger?', 'STUFFING_CHEASE');
//         }
//     },
//     getBurgerPriceAndCalories : function () {
//         this.validation(this.size, this.stuffing);
//         this.priceCalcAndCalories();
//         return `price of your burger`- ${this.price}
//             calories
//
//     }
// }
//
