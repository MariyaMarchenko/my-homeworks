// $(function(){
//
//     $('.tabs li').not('.active').click(function(){
//         let index = $(this).index();
//         let content = $('.tabs-content li').eq(index);
//         $(this).addClass('active').siblings().removeClass('active');
//         $('.tabs-content li').css('display', 'none').eq(index).css('display', 'block');
//     })
//
//     $('.tabs li:first').addClass('active');
//     $('.tabs-content li:first').css('display', 'block');
//
// })


$('.tabs li').click(function () {
    $(this).addClass('active').siblings().removeClass('active');
})


$('.tabs-title').on('click', (e) => {
    const dataValue = $(e.target).data('tab');
    $('.content').hide();
    $('.tabs-content').find(`[id = ${dataValue}]`).show();
})