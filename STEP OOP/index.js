// let createBtn = document.getElementById("createBtn");
// console.log(createBtn);
// const firstText = document.getElementById("dropText");
// console.log(firstText);
// const modalForm = document.getElementById("modalForm");

// createBtn.addEventListener("click",() => {
//     modalForm.style.display = "block";
// firstText.style.display ="none";
// document.getElementById('dropText').style.display = "none";
// document.getElementById('modalForm').style.display = "block";
// });


// createBtn.addEventListener("click", showModalForm);
//
// function showModalForm() {
//     let modalDiv = document.createElement('div');
//     modalDiv.className = "modal-div container";
//     document.body.appendChild(modalDiv);
//
//     let modalForm=document.createElement('form');

//     modalForm.className = "drop-div-form"
//     modalDiv.appendChild(modalForm);
// }

// showModalForm();

// class Doctor  {
//    constructor(name, card){
//        this.name = name;
//        this.card = card;
//    }
// }
//
// const doctorList = [
//      new Doctor("Терапевт", ["Цель Вашего визита","Ваше обычное давление"]),
//      new Doctor ("Стоматолог", ["Перенесенные заболевания сердечно-сосудистой системы"])
// ];


class Visit {
    constructor(name, purpose, date, description,) {
        this.name = name;
        this.purpose = purpose;
        this.date = date;
        this.description = description;
    }


    static createModal() {

        console.log("click");
        let firstText = document.getElementById("dropText");
        firstText.style.display = "none";
        let border = document.getElementById("borderBlock");
        let modalDiv = document.createElement('div');
        modalDiv.className = "modal-div container";
        border.appendChild(modalDiv);

        let modalForm = document.createElement("form");
        modalForm.className = "modal-form";

        let selectList = document.createElement("select");
        selectList.id = "selectDoctor";

        let option = document.createElement("option");
        option.innerHTML = "Выберите врача";
        let option1 = document.createElement("option");
        option1.innerHTML = "Кардиолог";
        let option2 = document.createElement("option");
        option2.innerHTML = "Стоматолог";
        let option3 = document.createElement("option");
        option3.innerHTML = "Терапевт";


        selectList.appendChild(option);
        selectList.appendChild(option1);
        selectList.appendChild(option2);
        selectList.appendChild(option3);


        let inputModal = document.createElement("textarea");
        inputModal.maxlength = "400";
        inputModal.className = "modal-input";

        let modalBtn = document.createElement('button');
        modalBtn.innerHTML = "Create";
        modalBtn.className = "modal-btn";
        modalBtn.id = "modalBtn";


        modalForm.appendChild(selectList);

        modalForm.appendChild(inputModal);
        modalForm.appendChild(modalBtn);

        modalDiv.appendChild(modalForm);
        this.disabled = true;


        let fragment = document.createElement("div");
        selectList.onchange = function () {

            if (this.selectedOptions[0].value === "Кардиолог") {
                while (fragment.firstChild) {
                    console.log(fragment.firstChild)
                    fragment.firstChild.remove()
                }


                console.log("test");
                let purpose = document.createElement("input");
                purpose.placeholder = "Цель Вашего визита";
                purpose.name = "Visit purpose";
                let pressure = document.createElement("input");
                pressure.placeholder = "Ваше обычное давление";
                pressure.name = "Visitor's pressure";
                let weight = document.createElement("input");
                weight.placeholder = "Ваш вес";
                weight.name = "Visitor's weight";
                let diseases = document.createElement("input");
                diseases.placeholder = "Перенесенные заболевания сердечно-сосудистой системы";
                diseases.name = "Visitor's diseases";
                let age = document.createElement("input");
                age.placeholder = "Ваш возраст";
                age.name = "Visitor's age";
                let name = document.createElement("input");
                name.placeholder = "Имя Фамилия врача";
                name.name = "name";
                let lastDate = document.createElement("input");
                lastDate.placeholder = "Последняя дата посещения специалиста";
                lastDate.name = "Visit date";


                fragment.appendChild(purpose);
                fragment.appendChild(pressure);
                fragment.appendChild(weight);
                fragment.appendChild(diseases);
                fragment.appendChild(age);
                fragment.appendChild(name);
                modalForm.insertBefore(fragment, inputModal);


            } else if (this.selectedOptions[0].value === "Стоматолог") {
                while (fragment.firstChild) {
                    fragment.firstChild.remove()
                }


                let purpose = document.createElement("input");
                purpose.placeholder = "Цель Вашего визита";
                purpose.name = "Visit purpose";
                let pressure = document.createElement("input");
                pressure.placeholder = "Ваше обычное давление";
                pressure.name = "Visitor's pressure";
                let weight = document.createElement("input");
                weight.placeholder = "Ваш вес";
                weight.name = "Visitor's weight";
                let diseases = document.createElement("input");
                diseases.placeholder = "Перенесенные заболевания сердечно-сосудистой системы";
                diseases.name = "Visitor's diseases";
                let age = document.createElement("input");
                age.placeholder = "Ваш возраст";
                age.name = "Visitor's age";
                let name = document.createElement("input");
                name.placeholder = "Имя Фамилия врача";
                name.name = "name";
                let lastDate = document.createElement("input");
                lastDate.placeholder = "Последняя дата посещения специалиста";
                lastDate.name = "Visit date";

                fragment.appendChild(purpose);
                fragment.appendChild(lastDate);
                fragment.appendChild(name);

                modalForm.insertBefore(fragment, inputModal);

            } else if (this.selectedOptions[0].value === "Терапевт") {
                while (fragment.firstChild) {
                    fragment.firstChild.remove();
                }
                let purpose = document.createElement("input");
                purpose.placeholder = "Цель Вашего визита";
                purpose.name = "Visit purpose";
                let pressure = document.createElement("input");
                pressure.placeholder = "Ваше обычное давление";
                pressure.name = "Visitor's pressure";
                let weight = document.createElement("input");
                weight.placeholder = "Ваш вес";
                weight.name = "Visitor's weight";
                let diseases = document.createElement("input");
                diseases.placeholder = "Перенесенные заболевания сердечно-сосудистой системы";
                diseases.name = "Visitor's diseases";
                let age = document.createElement("input");
                age.placeholder = "Ваш возраст";
                age.name = "Visitor's age";
                let name = document.createElement("input");
                name.placeholder = "Имя Фамилия врача";
                name.name = "name";
                let lastDate = document.createElement("input");
                lastDate.placeholder = "Последняя дата посещения специалиста";
                lastDate.name = "Visit date";

                fragment.appendChild(purpose);
                fragment.appendChild(age);
                fragment.appendChild(name);

                modalForm.insertBefore(fragment, inputModal);
            }


        };


        modalForm.appendChild(inputModal);
        modalForm.appendChild(modalBtn);

        modalDiv.appendChild(modalForm);
        this.disabled = true;



        modalBtn.addEventListener("click", (e) => {
            e.preventDefault();
            Visit.createCard(modalForm)
        });


    };


    static createCard(modalWindow) {


        let border = document.getElementById("borderBlock");
        let card = document.createElement('div');
        card.className = "card-wrap";
        border.appendChild(card);


        let removeCardBtn = document.createElement("span");
        removeCardBtn.innerHTML = "X";

        let showMoreBtn = document.createElement("button");
        removeCardBtn.class = "remove-card";
        showMoreBtn.innerHTML = "Показать больше";
        showMoreBtn.className = "card-button";

        showMoreBtn.id = "showMore";
        console.log("test");

        console.log(modalWindow.children);

        let doctorName = modalWindow.children[0].value;
        console.log(doctorName);


        let fields = modalWindow.children[1].children;

        let fullName =  document.querySelector( "input[name=name]").value;
        console.log(fullName);


        let ul = document.createElement("ul");
        let content = [].map.call(fields, input => {
            return input.name + " : " + input.value
        });
        console.log(content);

        content.forEach(contItem => {
            let li = document.createElement('li');
            li.innerText = contItem;
            ul.appendChild(li);
            ul.style.display = "none";

        });


        card.append(fullName);
        card.append(doctorName);
        card.append(ul);
        card.appendChild(showMoreBtn);

        showMoreBtn.addEventListener('click', () => {
            if (ul.style.display === "none") {
                ul.style.display = "block";
            }else {
                ul.style.display = "none"
            }
        })

    };


}

createBtn.addEventListener("click", Visit.createModal);

