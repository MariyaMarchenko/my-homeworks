
let savedDashBoardsString = localStorage.getItem('dashBoardCards');
if (savedDashBoardsString === null || savedDashBoardsString === undefined){
    localStorage.setItem('dashBoardCards', JSON.stringify([]));

}else {
    let savedDashBoards = JSON.parse(savedDashBoardsString);
    createDashBoardCards(savedDashBoards);

}

class Doctor  {
    constructor(name, mainCard, additionalCard){
        this.name = name;
        this.mainCard = mainCard;
        this.additionalCard = additionalCard;

    }
}

class Card {
    constructor(id,placeholder) {
        this.id = id;
        this.placeholder = placeholder;
    }
}

class BoardCard {
    constructor( mainInfo, additionalInfo){
        this.mainInfo = mainInfo;
        this.additionalInfo = additionalInfo;
    }
}


const doctorList = [
    new Doctor ("Кардиолог",  [{placeholder:"Имя Фамилия врача", id:"doctorFullName"}],[{placeholder:"Цель Вашего визита", id: "visitPurpose"},{placeholder: "Ваше обычное давление", id: "patientPressure"},{placeholder:"Ваш вес", id: "patientWeight"}, {placeholder:"Перенесенные заболевания сердечно-сосудистой системы", id:"patientDiseases"}, {placeholder:"Ваш возраст", id:"patientAge"},{placeholder:"Последняя дата посещения специалиста", id:"lastVisit"}]),
    new Doctor("Терапевт", [{placeholder:"Имя Фамилия врача", id:"doctorFullName"}], [{placeholder:"Цель Вашего визита", id:"visitPurpose"},{placeholder:"Ваш возраст", id:"patientAge"}]),
    new Doctor ("Стоматолог", [{placeholder:"Имя Фамилия врача", id:"doctorFullName"}], [{placeholder:"Цель Вашего визита", id: "visitPurpose"},{placeholder:"Последняя дата посещения специалиста", id:"lastVisit"}])
];


function createDashBoardCards(savedDashBoards) {
    savedDashBoards.forEach(savedDashBoard => createDashBoardCard(savedDashBoard));

}

 function  createDashBoardCard (dashBoardCard){
     let firstText = document.getElementById("dropText");
     firstText.style.display = "none";
let id = 0;
     let boardCard = document.createElement('div');
     boardCard.className = "card-wrap";
     boardCard.id = `card-wrap-${id}`;
     console.log(boardCard);
     let showMoreBtn = document.createElement('button');
     showMoreBtn.innerHTML = "Показать больше";
     showMoreBtn.className = "card-button";
     showMoreBtn.id = "showMoreBtn";
     let border = document.getElementById("borderBlock");
     let cardItemX = document.createElement("span");
     cardItemX.innerHTML = "X";
     cardItemX.className = "board-card-x";

// id++;

     // function visitUser(boardCard){
     //     boardCard.classList.add('disactive');
     // }
//      cardItemX.addEventListener("click", ()=>visitUser(boardCard));

     cardItemX.addEventListener("click", function () {
         // visitUser(boardCard)

         boardCard.remove()
         let cardWrap = document.querySelectorAll('.card-wrap');
         console.log(cardWrap);
         let currentIndex = 0;


         let savedDashBoards = JSON.parse(localStorage.getItem('dashBoardCards'));


         cardWrap.forEach(function (item, index) {
             if (event.target.parentNode === item)
                 currentIndex = index;
         });




         savedDashBoards.splice(currentIndex, 1);


         localStorage.setItem('dashBoardCards', JSON.stringify(savedDashBoards));
         if (savedDashBoards.length === 0) {
             let firstText = document.getElementById('dropText')
             firstText.style.display = 'block'
         }

         })


     border.appendChild(boardCard);
     boardCard.appendChild(cardItemX);
     makeDragonDrop(boardCard);
     dashBoardCard.mainInfo.forEach ( (item) => {
             let title = document.createElement("span");
             title.innerHTML = item;
             boardCard.appendChild(title);
         }

     );

        let additionalInfoBlock = document.createElement("div");

     dashBoardCard.additionalInfo.forEach((additionalItem) => {
             // let additionalInfoBlock = document.createElement("div");
             let additionalInfoItem = document.createElement("p");
             additionalInfoItem.innerHTML = additionalItem;
             console.log(additionalInfoItem);

             additionalInfoBlock.appendChild(additionalInfoItem);
             additionalInfoBlock.style.display = "none";

         }
     );

     boardCard.appendChild(additionalInfoBlock);
     boardCard.appendChild(showMoreBtn);


     showMoreBtn.addEventListener("click", function () {
         if (additionalInfoBlock.style.display === "none") {
             additionalInfoBlock.style.display = "block";
         }else {
             additionalInfoBlock.style.display = "none"
         }

     })

 }

    function createCard() {

        console.log("click");
        // let firstText = document.getElementById("dropText");
        // firstText.style.display = "none";
        let border = document.getElementById("borderBlock");
        let modalDiv = document.createElement('div');
        modalDiv.className = "modal-div container";
        border.appendChild(modalDiv);

        let modalForm = document.createElement("form");
        modalForm.className = "modal-form";

        let selectList = document.createElement("select");
        selectList.id = "selectDoctor";

        let option = document.createElement("option");
        option.innerHTML = "Выберите врача";
        option.selected =true;
        option.hidden = true;
        selectList.appendChild(option);

        doctorList.forEach(function (doctor) {
            let option = document.createElement("option");
            option.innerHTML = doctor.name;
            selectList.appendChild(option);
        });

        selectList.onchange = function(selectedDoc){

            for (let i = 0; i< doctorList.length; i++){
                if(doctorList[i].name === selectedDoc.target.value){
                    if (document.getElementById("cardItems") !== null) {
                        document.getElementById("cardItems").remove();
                    }

                    modalForm.appendChild(createCardItems(doctorList[i]));
                    break;
                }
            }
        };

        modalForm.appendChild(selectList);


        function createCardItems (doctor) {
            let  fragment = document.createElement("div");
            fragment.id = "cardItems";

            doctor.mainCard.forEach(function (cardItem) {
                let purpose = document.createElement("input");
                purpose.name = cardItem.id;
                purpose.placeholder = cardItem.placeholder;
                fragment.appendChild(purpose);

            });
            doctor.additionalCard.forEach(function (cardItem) {
                let purpose = document.createElement("input");
                purpose.name = cardItem.id;
                purpose.placeholder = cardItem.placeholder;
                fragment.appendChild(purpose);

            });

            let modalBtn = document.createElement('button');
            modalBtn.innerHTML = "Create";
            modalBtn.className = "modal-btn";
            modalBtn.id = "modalBtn";


            modalBtn.addEventListener('click', function (formResult) {
                formResult.stopPropagation();


                let firstText = document.getElementById("dropText");
                firstText.style.display = "none";


                let dashBoardCard = new BoardCard( [doctor.name], []  );

                doctor.mainCard.forEach(function (cardItem) {
                    dashBoardCard.mainInfo.push(modalForm.elements[cardItem.id].value);
                });


                doctor.additionalCard.forEach(function (cardItem) {
                    dashBoardCard.additionalInfo.push(modalForm.elements[cardItem.id].value);
                });


                let savedDashBoards = JSON.parse(localStorage.getItem('dashBoardCards'));
                console.log(savedDashBoards);
                savedDashBoards.push(dashBoardCard);
                localStorage.setItem('dashBoardCards', JSON.stringify(savedDashBoards));
                // let firstText = document.getElementById("dropText");
                // firstText.style.display = "none";
               modalDiv.style.display = "none";
               createDashBoardCard(dashBoardCard);

            });

            fragment.appendChild(modalBtn);

            return fragment;

         }


        let inputModal = document.createElement("textarea");
        inputModal.maxlength = "400";
        inputModal.className = "modal-input";


        modalForm.appendChild(inputModal);


        modalDiv.appendChild(modalForm);
        this.disabled = true

}
createBtn.addEventListener("click", createCard);

const desk=document.getElementById('borderBlock');

findCard();
function makeDragonDrop(cardTarget) {
    let card = cardTarget;
    function move(e) {
        let cord = card.getBoundingClientRect();
        let dek = desk.getBoundingClientRect();
        if ((cord.x - 20 - dek.x) < 0) {
            card.mousePositionX = e.clientX + card.offsetLeft - cord.width - 20;
        }
        if ((cord.y - 20 - dek.y) < 0) {
            card.mousePositionY = e.clientY - 20;
        }
        if (((dek.x + dek.width) - (cord.x + cord.width + 20)) < 0) {
            card.mousePositionX = (card.offsetLeft - dek.width) + e.clientX + 40;
        }
        if (((dek.y + dek.height) - (cord.y + cord.height + 20)) < 0) {
            card.mousePositionY = (card.offsetTop - dek.height) + e.clientY + 20;
        }
        card.style.transform = `translate(${e.clientX - card.mousePositionX}px,${e.clientY - card.mousePositionY}px)`;
    }
    card.addEventListener('mousedown', (e) => {
        if (card.style.transform) {
            const transforms = card.style.transform;
            const transformX = parseFloat(transforms.split('(')[1].split(',')[0]);
            const transformY = parseFloat(transforms.split('(')[1].split(',')[1]);
            card.mousePositionX = e.clientX - transformX;
            card.mousePositionY = e.clientY - transformY;
        } else {
            card.mousePositionX = e.clientX;
            card.mousePositionY = e.clientY;
        }
        document.addEventListener('mousemove', move);
    });
    document.addEventListener('mouseup', e => {
        let card = e.target.closest('.card-wrap');
        if(card){
            let id = card.getAttribute('data-ID');
            localStorage.removeItem(id);
            localStorage.setItem(id,card.outerHTML);
            console.log(id);
            document.removeEventListener('mousemove', move);
        }
        return false;
    });
}
function findCard() {
    if (document.querySelectorAll('.card-wrap').length) {
        document.querySelectorAll('.card-wrap').forEach((item) => {
            makeDragonDrop(item);
        })
    }
}

