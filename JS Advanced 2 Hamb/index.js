class Hamburger {
constructor(size, stuffing){
    this.size = size;
    this.stuffing = stuffing;
    this.toppings = [];
    this.price = size.price += stuffing.price;
    this.ccal = size.ccal += stuffing.ccal;
}

    get topping(){
        if (!this.toppings.length) return "NO TOPPINGS ADDED";
        return this.toppings
    }

    // getToppings(){
    //     if (!this.toppings.length) return "NO TOPPINGS ADDED";
    //     return this.toppings
    // }

    getStuffing() {
        return this.stuffing
    }

    getSize(){
        return this.size
    }

    addTopping(topping) {
        for (let topp of this.toppings) {
            if (topp.name === topping.name) throw new Error("topping already added");
        }

        this.toppings.push(topping);

    }

    removeTopping(topping) {
        try {
            if (this.toppings.length === 0) throw new HamburgerExeption("There are no toppings");
            let added = this.toppings.map(top => top.name);
            if(!topping) throw new HamburgerExeption("You are not added topping")
            if (!added.includes(topping.name)) throw  new HamburgerExeption("There is no such topping");
            this.toppings = this.toppings.filter(item => item.name !== topping.name)
        }catch (err) {
            console.log(err.name + ": " + err.message);
        }
    }

    calculatePrice() {
        let arrTopp = this.toppings
        let price = 0
        arrTopp.forEach(function (top) {
            price += top.price
        })
        return price + this.price
    }

    calculateCalories() {
        let arrayTopp = this.toppings
        let ccal = 0
        arrayTopp.forEach(function (topp) {
            ccal += topp.ccal
        })

        return ccal += this.ccal
    }
    static SIZE_SMALL = {
    name: 'small',
    price: 50,
    ccal: 20
};
    static SIZE_LARGE = {
    name: 'large',
    price: 100,
    ccal: 40
};

    static STUFFING_CHEESE = {
        name: 'cheese',
        price: 10,
        ccal: 20
    };

    static STUFFING_SALAD = {
        name: 'salad',
        price: 20,
        ccal: 5
    };

    static STUFFING_POTATO = {
        name: 'potato',
        price: 15,
        ccal: 10
    };

    static TOPPING_SPICE = {
        name: 'spice',
        price: 15,
        ccal: 0
    };

    static TOPPING_MAYO = {
        name: 'mayo',
        price: 20,
        ccal: 5
    };

}

// function HamburgerExeption(msg){
//     this.name = "Hamburger Exeption";
//     this.message = msg;
// }


class HamburgerExeption extends Error {
    constructor(msg) {
        super(msg)
        this.name = "Hamburger Exeption";
        this.message = msg;
    }
}


Hamburger.STUFFING_CHEESE = {
    name: 'cheese',
    price: 10,
    ccal: 20
};

Hamburger.STUFFING_SALAD = {
    name: 'salad',
    price: 20,
    ccal: 5
};

Hamburger.STUFFING_POTATO = {
    name: 'potato',
    price: 15,
    ccal: 10
};

Hamburger.TOPPING_SPICE = {
    name: 'spice',
    price: 15,
    ccal: 0
};

Hamburger.TOPPING_MAYO = {
    name: 'mayo',
    price: 20,
    ccal: 5
};


let burger = new Hamburger(Hamburger.SIZE_LARGE, Hamburger.STUFFING_SALAD);
burger.addTopping(Hamburger.TOPPING_SPICE);
console.log(burger.getStuffing());
console.log (burger.getSize());
// console.log (burger.getToppings());
console.log ('===========', burger.topping);
console.log(burger.calculatePrice());
console.log(burger.calculateCalories());
console.log (burger.removeTopping(Hamburger.TOPPING_SPICE));

// console.log (burger.removeTopping(Hamburger.TOPPING_MAYO));
console.log(burger.calculatePrice());
console.log(burger.calculateCalories());

console.log(burger);

console.dir(Error)


