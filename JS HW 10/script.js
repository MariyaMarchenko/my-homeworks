
function showPassword() {
    this.classList.toggle('fa-eye-slash');
    const input = document.getElementById(this.getAttribute("data-password"));
    input.type = input.type === "password" ? "text" : "password";

}

const icons = document.getElementsByClassName('icon-password');
for (let i = 0; i < icons.length; i++) {
    icons[i].addEventListener('click', showPassword)
}


function handleSubmit(e) {
    e.preventDefault();
    const password = document.getElementById('password').value;
    const confirmPassword = document.getElementById('confirmPassword').value;
    const hidden = document.getElementById('hidden-text');
    if (password && password === confirmPassword) {
        if (!hidden.classList.contains("d-none")){
            hidden.classList.add("d-none");
        }
        alert('You are welcome');
    } else {
        hidden.classList.remove("d-none")
    }
}


document.getElementById("main").addEventListener("submit", handleSubmit);
