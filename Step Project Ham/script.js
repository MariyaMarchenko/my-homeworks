// const serviceList = [{
// //     id: "webDesign",
// //     img: "img/",
// //     text: "lorem"
// // }];
// //
// // function onServiceClick(id) {
// //     let service;
// //
// //     for (currentService of serviceList){
// //         if (currentService.id === id){
// //             service=currentService;
// //             break;
// //         }
// //     }
// //     document.getElementById('serviceDescription').innerText=service.text;


let titles = document.getElementsByClassName("services-list-item");
let contents = document.getElementsByClassName("services-list-content");


function bindAll() {
    for (let i = 0; i < titles.length; i++) {
        titles[i].addEventListener('click', manageTabs);

    }
}

function manageTabs(event) {

    for (let i = 0; i < titles.length; i++) {
        titles[i].classList.remove("active");
        contents[i].classList.remove("active");

    }

    event.target.classList.add("active");
    let id = event.currentTarget.getAttribute('data-tab');
    document.getElementById(id).classList.add("active");
}

bindAll()


// $('.all').on('click', (elem) => {
//     let elemValue = $(elem.target).data('cont');
//     $('.amazing-form').show();
// });
// $('.graphic, .web, .landing, .word-press').on('click', (elem) => {
//     let elemValue = $(elem.target).data('cont');
//     $('.amazing-form').hide();
//     console.log($('.amazing-form'));
//     console.log($(elem.target).data('cont'));
//
//     $('.amazing-block-image').find(`[data-cont='${elemValue}']`).show();
// });


let categories = document.getElementsByClassName("amazing-block-list-item");
//
let categoriesContent = document.getElementsByClassName("amazing-form");


function bindCategory() {
    for (let i = 0; i < categories.length; i++) {
        categories[i].addEventListener("click", showContent);
    }
}

function showContent(event) {
    for (let i = 0; i < categories.length; i++) {
        categories[i].classList.remove('active');
    }

    event.target.classList.add('active');
    let keyword = event.target.classList[1];
    console.log(keyword);

    for (let i = 0; i < categoriesContent.length; i++) {
        categoriesContent[i].classList.remove('active');
    }
    for (let i = 0; i < categoriesContent.length; i++) {
        if (categoriesContent[i].getAttribute('data-cont') === keyword) {
            categoriesContent[i].classList.add('active');
        } else if (categoriesContent[i].getAttribute('data-all') === keyword){
            categoriesContent[i].classList.add('active');
        }

    }
}

bindCategory();

// let but =  document.getElementById('amazing-but');
//
//     but.addEventListener("click", function () {
//         let imgMoreItemCollection = document.querySelectorAll("img-more-item");
//         for (let i = 0; i < imgMoreItemCollection.length; i++) {
//             imgMoreItemCollection[i].style.display="block";
//         }
//     });

$(".amazing-block-button").on("click", (elem) => {
    $(".spinner, .img-more-item").show();
    $(".amazing-block-button, .spinner, .spinner:after").remove();
});


// $('.all-photo').on('click', (elem) => {
//     let dataValue = $(elem.target).data('content');
//     $('.amazing-work-img').show();
// });
// $('.graphic-photo, .web-photo, .landing-photo, .wordpress-photo').on('click', (elem) => {
//     let dataValue = $(elem.target).data('content');
//     $('.amazing-work-img').hide();
//     $('.amazing-work-photo').find(`[data-content='${dataValue}']`).show();
// });


let sliderSmallImg = document.getElementsByClassName('slider-img-item');
let sliderContent = document.getElementsByClassName("slider-block-items");

console.log('test', sliderSmallImg);
console.log('test', sliderContent);


function bindSlider() {
    for (let i = 0; i < sliderSmallImg.length; i++) {
        sliderSmallImg[i].addEventListener('click', manageSlider);
    }
}
let slydeIndex = 0;
let slyderArray = Array.from(sliderContent);
console.log(sliderContent);
console.log(slyderArray);

function manageSlider(event) {
    for (let i = 0; i < sliderSmallImg.length; i++) {
        sliderSmallImg[i].classList.remove('active');
    }
    event.target.classList.add("active");
    let key = event.target.classList[1];


    for (let i = 0; i < sliderContent.length; i++) {
        sliderContent[i].classList.remove("active");

    }


    for (let i = 0; i < sliderContent.length; i++) {
        if (sliderContent[i].getAttribute('data-slider') === key) {
            slydeIndex = i;
            sliderContent[i].classList.add('active');
            console.log(slydeIndex);

        }
    }
}

bindSlider();

let prevArrow = document.getElementById('prev');
let nextArrow = document.getElementById('next');

function showSlide(index) {

    if (index > sliderContent.length - 1) {
        index  = slydeIndex  = 0;
    }
    if (index < 0){
        index  = slydeIndex = sliderContent.length-1;
    }

    for (let i = 0; i < sliderSmallImg.length; i++) {
        sliderSmallImg[i].classList.remove('active');
    }
    sliderSmallImg[index].classList.add("active");


    for (let i = 0; i < sliderContent.length; i++) {
        sliderContent[i].classList.remove('active');
    }
    sliderContent[index].classList.add("active");
}

nextArrow.addEventListener('click', ()=>showSlide(++slydeIndex));


prevArrow.addEventListener('click', ()=> showSlide(--slydeIndex));



