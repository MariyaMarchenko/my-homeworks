let titles = document.getElementsByClassName("tabs-title");
let contents = document.getElementsByClassName("content");
console.log(contents);

function bindAll() {
    for (i = 0; i < titles.length; i++) {
        titles[i].addEventListener('click', manageTabs);
    }

}

function manageTabs(event) {
    for (i = 0; i < titles.length; i++) {
        titles[i].classList.remove("active");
        contents[i].classList.remove("active");
    }
    //при нажатии каждой  li присваиваем ей класс active
    event.target.classList.add("active");
    //из дата-атрибута получаем id, который соответствует контенту data-tab
    let id = event.currentTarget.getAttribute('data-tab');

    document.getElementById(id).classList.add("active");

}

bindAll();