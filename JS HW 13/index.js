if (!localStorage.getItem('theme')) {
    localStorage.setItem('theme', 'css/style.css')
}

const blueBtn = document.getElementById('blueTheme');
const redBtn = document.getElementById('redTheme');
const cssStyle = document.getElementById('pageTheme');
let hrefAttr = cssStyle.getAttribute('href');
hrefAttr = cssStyle.setAttribute('href', localStorage.getItem('theme'));

blueBtn.addEventListener('click', changeColor);
redBtn.addEventListener('click', changeColor);



function changeColor() {
    if (localStorage.getItem('theme') === 'css/red-style.css') {
        localStorage.setItem('theme', 'css/style.css');
        cssStyle.setAttribute('href', localStorage.getItem('theme'))

    } else {
        localStorage.setItem('theme', 'css/red-style.css');
        cssStyle.setAttribute('href', localStorage.getItem('theme'));
        console.log(localStorage.getItem('theme'));
    }

}



// localStorage.clear()